This is a library that enables you to create very easy to use syntax driven autocomplete friendly advanced searches.

Instead of having a 2 different input and select and checkboxes in a huge ugly form, you can have a simple language.

This library parses the queries so that it is easier for you to analyzing what the user is searching for.

This library parses the queries to enable you to provide autocomplete and error messages to your users.

This javascript is probably best for the server side, if you want to use autocomplete.


To test it do the following:

  cp queryconf-sample.js  queryconf.js 
  view index.html on you browser and type something in for a search. 
    The sample isn't with autocomplete and just shows you the parser response
