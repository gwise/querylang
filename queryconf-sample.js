var query_bracket_types = [
  "my tags"
  , "author"
  , "begin"
  , "end"
  , "tag"
];
var query_bracket_type_info = {
    "my tags" : 0
  , "begin" : "Begin date"
  , "end" : "End date"
  , "tag" : "Tag name"
};

// returns an array of keyword for autocomplete based upon the keyword_substring
// you'll need to either prepopulate this on your own or more likely make it query a database.
function query_keyword_autocomplete_lookup(keyword_substring){
  if ( keyword_substring ){
     return ["Environmental", "Foreign", "Irrigation", "Ohio", "Washington"];
  }
}

// returns an array of bracket_params based upon the param_type and param_substring.
// you'll need to either prepopulate this on your own or make it query a database.
function query_bracket_params_autocomplete_lookup(param_type, param_substring){
  // if ( param[param_type][param_substring] ){
  return [
    "Hello", "World", "These are", "Some sample", "Parameters!" 
  ];
  // } else { // make an ajax call to populate that datastructure and return // }
}

// this provides a nice empty string pull down for the autocomplete.
// this would need to be called directly by your autocomplete system in the event that there is no query
function query_empty_autocomplete(){
    var autocomplete = [];
    for(var i=0; i < query_bracket_types.length; i++){
      autocomplete.push("[" + query_bracket_types[i] + " " + query_bracket_params_autocomplete_lookup(query_bracket_types[i], '')[0] + "]");
    }
    return autocomplete;
}

// console.log(JSON.stringify(query_run({'str' : "Hello world [My Tags] [Tag]"})));
